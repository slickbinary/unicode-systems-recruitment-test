import React, { Component, Fragment } from 'react';
import { Container, Row, Col, Form, Input, Label, Button } from 'reactstrap';
import { FormErrors } from '../component/formsErrors';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            formErrors: { email: '', password: '' },
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateData(name, value) });
    }

    validateData(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateonsubmit);
    }

    validateonsubmit() {
        
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }

    handleSubmit(e){
        e.preventDefault();
        alert('::Submit sucessfully:')
        let data =[
            {
                email: this.state.email,
                password: this.state.password
            }
        ]
        console.log(data)
    }
    errorCheck(error) {
        return (error.length === 0 ? '' : 'has-error');
    }

    render() {
        return (
            <Fragment>
                <Container>
                    <Row>
                        <Col md={{ size: 6, offset: 3 }}>
                            <div className="cardmainfroms">
                                <Form className="vaildatefrom" onSubmit={this.handleSubmit}>

                                    <div className={`form-group ${this.errorCheck(this.state.formErrors.email)}`}>
                                        <Label htmlFor="email">Email address</Label>
                                        <Input type="email" required className="form-control" name="email"
                                            placeholder="Email"
                                            value={this.state.email}
                                            onChange={this.handleChange} />
                                    </div>
                                    <div className={`form-group ${this.errorCheck(this.state.formErrors.password)}`}>
                                        <Label htmlFor="password">Password</Label>
                                        <Input type="password" className="form-control" name="password"
                                            placeholder="Password"
                                            value={this.state.password}
                                            onChange={this.handleChange} />
                                    </div>
                                    <div className="erroemain">
                                        <FormErrors formErrors={this.state.formErrors} />
                                    </div>
                                    <Button type="submit" className="btn-main" disabled={!this.state.formValid} onSubmit={this.handleSubmit}>Sign up</Button>
                                </Form>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

export default Home;
